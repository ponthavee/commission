<?php

    class CalCommission{
       public $number;
       public $cost;
       public $lucre;
       public $num;
       public $commValue;
       public $total;
     
         function Commission($number){
            if($number <= 5000){
                $commValue = 0;
            }else if(($number >=5001) && ($number <= 50000)){
                //$commValue = "5%";
                $commValue = $this->calPercentage($number,5);
            }else if(($number >=50001) && ($number <=300000)){
                //$commValue = "7%";
                $commValue = $this->calPercentage($number,7);
            }else if(($number >=300001) && ($number <=600000)){
                //$commValue = "10%";
                $commValue = $this->calPercentage($number,10);
            }else if($number >=600001){
                //$commValue = "13%";
                $commValue = $this->calPercentage($number,13);
            }
            return @number_format($commValue,2);
        }

        function percentage($cost, $lucre){  //คำนวณหากำไรจากยอดขาย
            
            $num = @($lucre/$cost)*100; //เอากำไรมาหารกับยอดขายแล้วคูณด้วย 100 จะได้ออกมาเป็น %
            $total = number_format($num,2); //ใช้ function number_format เพื่อแปลงจำนวนให้มีทศนิยม 2 ตำแหน่ง
            //echo "กำไร ".$total."%";
        
            switch($total){    
                case (($total >= 5) && ($total <=9.99)):  // เช็คเงื่อนไขว่า กำไร 5 - 9.99 ได้ค่าคอมมิชชั่น  = 1% 
                $commValue = $this->calPercentage($cost,1); // หลังจากเช็คได้แล้วเอามาเข้า function CalPercentage() 
                break;
                case (($total >= 10) && ($total <=19.99)):  
                    $commValue = $this->calPercentage($cost,3); 
                break; 
                case (($total >= 20) && ($total <=29.99)):    
                    $commValue = $this->calPercentage($cost,5);
                break;
                case ($total >= 30):    
                    $commValue = $this->calPercentage($cost,7);
                break;
                default:   
                $commValue = "No Commission";                 
                }
            
                return @number_format($commValue,2);
        }

        function calPercentage($cost,$percen){ // function คำนวณหาค่า คอมมิชชั่น โดยมี Parameter 2 ตัวคือ ยอดขาย กับ ค่าคอมิชชั่น เพื่อหาค่าคอมมิชชั่นที่จะจ่ายให้กับพนักงาน
             $total = ($cost*$percen)/100;
             return $total;
        }

    }


?>