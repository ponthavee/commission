<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="bulma.min.css" />
 
    <title>Document</title>
    <style>
        .title.is-1 {
                font-size: 1.3rem;
        }
    </style>
    <script>
            function checkRdo(numRdo){               
                var x = document.getElementById("fieldLucre");   // Get the element txtPercentage"   

                if(numRdo == 1){ // if numRdo == 1 add css display:none
                    x.style.display = "none";
            }else{
                    x.style.display = "block";
            }
        }
            
    </script>
</head>
<?php
        require("CalCommission.php");
        
        $rsCommision = new CalCommission();

        //กำหนดวันที่ในรูปแบบ Array
        $arr = array("7","10","14","20","25");

        //เช็คว่ามีการกดปุ่ม submit หรือไม่
        if(isset($_POST['btnSubmit'])){   
              //เช็คเงื่อนไขใช้ในการคำนวณ    
            if($_POST['rdoCondition'] == '1'){           
                $commission = $rsCommision->Commission($_POST['txtCal']);
             }else{                       
                $commission = $rsCommision->percentage($_POST['txtCal'], $_POST['txtPercentage']);
                //echo 'ค่าคอมที่ต้องจ่าย'.$commission;
            }
        }

?>
<body>

     <section class="container">
		<div class="hero-body">
			<div class="container has-text-centered">
				<div class="columns is-6 is-variable ">
					<div class="column is-two-thirds has-text-left">
						<h3 class="title is-1">โปรแกรมคำนวณค่าคอมมิชชั่นการขายสินค้า</h1>
						<p class="is-size-6">ผลลัพธ์ที่ต้องการให้แสดง(Output)</p>
                        <p>1. จำนวนวันที่จ่ายค่าคอมมิชชั่น มีหน่วยเป็นวัน <span style="color:red; font-weight:bold;">(<?php echo((@$_POST['selDuration']=='')?'':$_POST['selDuration']); ?> วัน)</span></p> 
                        <p>2. แสดงยอดขายมีหน่วยเป็นบาท <span style="color:red; font-weight:bold;">(<?php echo((@$_POST['txtCal'] == '')?'':number_format($_POST['txtCal'],2)." บาท"); ?> )</span></p>
                        <p>3. แสดงค่าคอมมิชชั่นที่ต้องจ่ายให้พนักงาน มีหน่วยเป็นบาท <span style="color:red; font-weight:bold;">(<?php echo((@$commission == '')?'':$commission." บาท"); ?> )</span></p>
                    </div>
					<div class="column is-one-third has-text-left">
                    <form method="post">
                    <div class="field">
					<label class="label">ราคาทุน</label>
                    <div class="control">
                    <label class="radio">
                        <input type="radio" name="rdoCondition"  id="rdo1" value="1" onclick="checkRdo(1)" checked> ใช้ยอดขายเป็นเกณฑ์
                    </label>
                    </div>
                    <div class="control">
                    <label class="radio">
                        <input type="radio" name="rdoCondition" id="rdo2" value="2" onclick="checkRdo(2)" > ใช้กำไรเป็นเกณฑ์
                    </label>
                    </div>
                    </div>
						<div class="field">
							<label class="label">จำนวนวัน</label>
							<div class="control">                            
                                <div class="select">
                                    <select name="selDuration">
                                   <?php foreach ($arr as $key => $value) {                
                                        echo "<option value=".$value.">$value</option>";
                                     } ?>
                                    </select>
                                </div>

							</div>
						</div>
						<div class="field">
							<label class="label">ยอดขาย</label>
							<div class="control">
								<input class="input is-medium" type="text" name="txtCal">
							</div>
						</div>
                        
                        <div class="field" id="fieldLucre" style="display:none;" >
							<label class="label">กำไร</label>
							<div class="control">
								<input class="input is-medium" type="text" name="txtPercentage" >
							</div>
						</div>             
						<div class="control">
							<button type="submit" name="btnSubmit"  class="button is-link is-fullwidth has-text-weight-medium is-medium">Calculator</button>
						</div>
                    </form>
					</div>
				</div>
			</div>
		</div>
	</section>
    
</body>
</html>